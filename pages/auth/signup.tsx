import React from 'react'
import { useRouter } from 'next/router'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'

import useAuth from '../../hooks/useAuth'

import AuthCTA from '../../components/AuthCTA'
import AuthHead from '../../components/AuthHead'
import CheckboxInput from '../../components/CheckboxInput'
import TextInput from '../../components/TextInput'

interface IFormValues {
  name: string
  email: string
  password: string
  remember: boolean
}

const SignUpSchema = Yup.object().shape({
  name: Yup.string().required('Please enter your name'),
  email: Yup.string()
    .email('Please enter a valid email')
    .required('Please enter your email'),
  password: Yup.string()
    .min(8, 'Must be at least 8 characters long')
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]$/,
      'Must contain 1 uppercase, 1 lowercase, 1 number & 1 special character',
    )
    .required('Please enter your password'),
  remember: Yup.boolean(),
})

const Login: React.FC = () => {
  const router = useRouter()
  const { error, signUp } = useAuth()

  const handleSignUp = async (values: IFormValues): Promise<void> => {
    const { name, email, password, remember } = values
    const response = await signUp(name, email, password, remember)
    if (response?.user) router.push('/')
  }

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <AuthHead
          title="Sign up for a new account"
          subtitle="sign in to your account"
          link="/auth/signin"
        />
        <Formik
          initialValues={{ name: '', email: '', password: '', remember: true }}
          validationSchema={SignUpSchema}
          onSubmit={handleSignUp}
        >
          {({ isValid }) => (
            <Form className="mt-8 space-y-6">
              <div className="space-y-2">
                <div className="rounded-md shadow-sm space-y-2">
                  <TextInput
                    label="Name"
                    name="name"
                    type="text"
                    autoComplete="given-name"
                    placeholder="Name"
                  />
                  <TextInput
                    label="Email Adress"
                    name="email"
                    type="email"
                    autoComplete="email"
                    placeholder="Email address"
                  />
                  <TextInput
                    label="Password"
                    name="password"
                    type="password"
                    autoComplete="current-password"
                    placeholder="Password"
                  />
                </div>
                {error && (
                  <div className="text-xs text-red-600 ml-1">{error}</div>
                )}
              </div>

              <div className="flex items-center justify-between">
                <CheckboxInput label="Remember me" name="remember" />
              </div>

              <AuthCTA text="Sign Up" disabled={isValid} />
            </Form>
          )}
        </Formik>
      </div>
    </div>
  )
}

export default Login
