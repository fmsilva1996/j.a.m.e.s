import type { AppProps } from 'next/app'
import 'tailwindcss/tailwind.css'

import AuthProvider from '../components/AuthProvider'

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <AuthProvider>
      <Component {...pageProps} />
    </AuthProvider>
  )
}
export default MyApp
