import Head from 'next/head'

import useRequireAuth from '../hooks/useRequireAuth'

import Navbar from '../components/Navbar'

const Home: React.FC = () => {
  const auth = useRequireAuth()

  return (
    <div>
      <Head>
        <title>Workflow</title>
        <meta name="description" content="just A Moderately Efficient System" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Navbar />
        <pre>{JSON.stringify(auth.user?.providerData[0], null, 2)}</pre>
      </main>
    </div>
  )
}

export default Home
