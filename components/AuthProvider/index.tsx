import React from 'react'

import useProvideAuth, { IAuth } from '../../hooks/useProvideAuth'

export const AuthContext = React.createContext<IAuth>({} as IAuth)

const AuthProvider: React.FC = ({ children }) => (
  <AuthContext.Provider value={useProvideAuth()}>
    {children}
  </AuthContext.Provider>
)

export default AuthProvider
