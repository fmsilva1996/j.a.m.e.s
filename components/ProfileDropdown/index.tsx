import { Fragment } from 'react'
import { useRouter } from 'next/router'
import Avatar from 'boring-avatars'

import useRequireAuth from '../../hooks/useRequireAuth'
import { classNames } from '../../lib/helpers/styles'

import { Menu, Transition } from '@headlessui/react'

const ProfileDropdown: React.FC = () => {
  const router = useRouter()
  const auth = useRequireAuth()

  const handleSignOut = async (
    e:
      | React.MouseEvent<HTMLAnchorElement>
      | React.KeyboardEvent<HTMLAnchorElement>,
  ): Promise<void> => {
    e.preventDefault()
    await auth.signOut()
    router.push('/auth/signin')
  }

  return (
    <Menu as="div" className="ml-3 relative">
      {({ open }) => (
        <>
          <div>
            <Menu.Button className="bg-gray-800 flex text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
              <span className="sr-only">Open user menu</span>
              <Avatar
                size="32"
                variant="bauhaus"
                colors={['#001848', '#301860', '#483078', '#604878', '#906090']}
              />
            </Menu.Button>
          </div>
          <Transition
            show={open}
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Menu.Items
              static
              className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
            >
              <Menu.Item>
                {({ active }) => (
                  <a
                    role="button"
                    tabIndex={0}
                    className={classNames(
                      active ? 'bg-gray-100 cursor-pointer' : '',
                      'block px-4 py-2 text-sm text-gray-700',
                    )}
                    onClick={handleSignOut}
                    onKeyDown={handleSignOut}
                  >
                    Sign out
                  </a>
                )}
              </Menu.Item>
            </Menu.Items>
          </Transition>
        </>
      )}
    </Menu>
  )
}

export default ProfileDropdown
