import React from 'react'
import { useField } from 'formik'

interface InputProps {
  label: string
  name: string
}

const CheckboxInput: React.FC<InputProps> = ({ label, name }) => {
  const [field] = useField({ name, type: 'checkbox' })

  return (
    <div className="flex items-center">
      <input
        id={name}
        {...field}
        className="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded"
        type="checkbox"
      />
      <label htmlFor={name} className="ml-2 block text-sm text-gray-900">
        {label}
      </label>
    </div>
  )
}

export default CheckboxInput
