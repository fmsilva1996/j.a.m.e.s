import React from 'react'
import { useField } from 'formik'

interface InputProps {
  label: string
  name: string
  type: string
  autoComplete?: string
  placeholder: string
}

const TextInput: React.FC<InputProps> = ({
  label,
  type,
  name,
  autoComplete,
  placeholder,
}) => {
  const [field, meta] = useField(name)

  return (
    <div>
      <label htmlFor={name} className="sr-only">
        {label}
      </label>
      <input
        id={name}
        type={type}
        autoComplete={autoComplete}
        placeholder={placeholder}
        className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
        {...field}
      />
      <div className="text-xs text-red-600 ml-1">
        {meta.touched && meta.error && meta.error}
      </div>
    </div>
  )
}

export default TextInput
