import React from 'react'
import Link from 'next/link'

interface AuthHeadProps {
  title: string
  subtitle: string
  link: string
}

const AuthHead: React.FC<AuthHeadProps> = ({ title, subtitle, link }) => (
  <div>
    <img
      className="mx-auto h-12 w-auto"
      src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
      alt="Logo"
    />
    <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
      {title}
    </h2>
    <p className="mt-2 text-center text-sm text-gray-600">
      Or{' '}
      <Link href={link}>
        <a className="font-medium text-indigo-600 hover:text-indigo-500">
          {subtitle}
        </a>
      </Link>
    </p>
  </div>
)

export default AuthHead
