import { useContext } from 'react'

import { AuthContext } from '../components/AuthProvider'
import { IAuth } from './useProvideAuth'

const useAuth = (): IAuth => useContext(AuthContext)

export default useAuth
