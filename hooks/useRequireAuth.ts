import { useEffect } from 'react'
import { useRouter } from 'next/router'

import useAuth from './useAuth'
import { IAuth } from './useProvideAuth'

const useRequireAuth = (redirectUrl = '/auth/signin'): IAuth => {
  const router = useRouter()
  const auth = useAuth()

  useEffect(() => {
    if (!auth.loading && !auth.user) router.push(redirectUrl)
  }, [auth, router, redirectUrl])

  return auth
}

export default useRequireAuth
