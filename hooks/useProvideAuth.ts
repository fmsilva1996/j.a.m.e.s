import { useEffect, useState } from 'react'
import firebase from 'firebase/app'

import firebaseApp from '../firebase/firebase'

export interface IAuth {
  user: firebase.User | null
  error: string
  loading: boolean
  signUp: (
    email: string,
    password: string,
    name: string,
    rememberMe: boolean,
  ) => Promise<void | firebase.auth.UserCredential>
  signIn: (
    email: string,
    password: string,
    rememberMe: boolean,
  ) => Promise<void | firebase.auth.UserCredential>
  signOut: () => Promise<void>
}

const SESSION = firebase.auth.Auth.Persistence.SESSION
const LOCAL = firebase.auth.Auth.Persistence.LOCAL

const useProvideAuth = (): IAuth => {
  const [user, setUser] = useState<firebase.User | null>(null)
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const unsubscribe = firebaseApp.auth().onAuthStateChanged(firebaseUser => {
      if (firebaseUser) {
        setUser(firebaseUser)
        setLoading(false)
      } else setUser(null)
    })

    return () => unsubscribe()
  }, [])

  const signUp: IAuth['signUp'] = async (name, email, password, rememberMe) => {
    try {
      await firebase.auth().setPersistence(rememberMe ? LOCAL : SESSION)
      const signUpResponse = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
      await signUpResponse.user?.updateProfile({ displayName: name })
      return signUpResponse
    } catch (err) {
      setError(err.message)
    }
  }

  const signIn: IAuth['signIn'] = async (email, password, rememberMe) => {
    try {
      await firebase.auth().setPersistence(rememberMe ? LOCAL : SESSION)
      const signInResponse = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
      return signInResponse
    } catch (err) {
      setError(err.message)
    }
  }

  const signOut: IAuth['signOut'] = async () => {
    const signOutResponse = await firebase.auth().signOut()
    return signOutResponse
  }

  return { user, error, loading, signUp, signIn, signOut }
}

export default useProvideAuth
